package exercise1;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class assignment1 {
    public static class Map extends Mapper<LongWritable, Text, Text, IntWritable> {
        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String line = value.toString();
            StringTokenizer tokenizer = new StringTokenizer(line);
            while (tokenizer.hasMoreTokens()) {
                word.set(tokenizer.nextToken());
                context.write(word, one);
            }
        }
    }

    public static class Reduce extends Reducer<Text, IntWritable, Text,IntWritable> {
        private IntWritable intwritable_value = new IntWritable(0);
        @Override
        protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int sum = 0;
            for (IntWritable value : values)
                sum += value.get();

            intwritable_value.set(sum); 
            context.write(key,intwritable_value);
        }
    }
    
    public static class MyMap extends Mapper<LongWritable ,Text, IntWritable,Text> {
        private Text word = new Text();
        String text="";
        String freq_to_string="";
        int freq_to_number;

        protected void map(LongWritable key,Text value,  Context context) throws IOException, InterruptedException {
            String line = value.toString();
            StringTokenizer tokenizer = new StringTokenizer(line);
            while (tokenizer.hasMoreTokens()) {
            	text=tokenizer.nextToken(); 
            	freq_to_string=tokenizer.nextToken();
                freq_to_number=Integer.parseInt(freq_to_string);
                if(freq_to_number>4000){
                	word.set(text);
                	context.write(new IntWritable(freq_to_number),word);
                }
            }
        }
    }

    public static class MyReduce extends Reducer<IntWritable,Text, Text,IntWritable> {        
    	protected void reduce( IntWritable key,Iterable<Text> values, Context context) throws IOException, InterruptedException {
            for (Text value : values){
            	context.write(value,key);
            }
    	}
    }

    public static class IntComparator extends WritableComparator {
    	public IntComparator(){
    		super(IntWritable.class, true);
    	}
    	@Override
        public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2) {
            Integer v1 = ByteBuffer.wrap(b1, s1, l1).getInt();
            Integer v2 = ByteBuffer.wrap(b2, s2, l2).getInt();
            return v1.compareTo(v2) * (-1);
        }
    }

	@SuppressWarnings({ "deprecation" })
	public static void main(String[] args) throws Exception {
		FileUtils.deleteDirectory(new File("/home/cloudera/workspace/Assignments/output"));
		FileUtils.deleteDirectory(new File("/home/cloudera/workspace/Assignments/output1"));

		int success;
		Configuration conf1 = new Configuration();
		Job job1 = new Job(conf1, "wordcount");

		job1.setJarByClass(assignment1.class);
		job1.setOutputKeyClass(Text.class);
		job1.setOutputValueClass(IntWritable.class);
		job1.setMapperClass(Map.class);
		job1.setReducerClass(Reduce.class);
		job1.setInputFormatClass(TextInputFormat.class);
		job1.setOutputFormatClass(TextOutputFormat.class);
		job1.setNumReduceTasks(1);

		String names = args[0];
		for (int i = 1; i <= 5; i++) {
			names = args[i] + "," + names;
		}
		System.out.println(names);
		FileInputFormat.setInputPaths(job1, names);
		FileOutputFormat.setOutputPath(job1, new Path(args[6]));
		success = job1.waitForCompletion(true) ? 1 : 0;
		if (success == 1) {
			System.out.println(success);

			// -----------JOB1------------
			Configuration conf2 = new Configuration();
			Job job2 = new Job(conf2, "frequence");

			job2.setJarByClass(assignment1.class);
			job2.setOutputKeyClass(IntWritable.class);
			job2.setOutputValueClass(Text.class);
			job2.setMapperClass(MyMap.class);
			job2.setSortComparatorClass(IntComparator.class);
			job2.setReducerClass(MyReduce.class);
			job2.setInputFormatClass(TextInputFormat.class);
			job2.setOutputFormatClass(TextOutputFormat.class);
			job2.setNumReduceTasks(1);

			FileInputFormat.setInputPaths(job2,"/home/cloudera/workspace/Assignments/output/part-r-00000");
			FileOutputFormat.setOutputPath(job2, new Path(args[7]));
			success = job2.waitForCompletion(true) ? 1 : 0;
			if (success == 1) {
				System.out.println(success);
			}
		}
	}
}