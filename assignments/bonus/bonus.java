package bonus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.TreeSet;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class bonus {
	static ArrayList<String> stopwords_array=null;	
	
	public static class PairsMap extends Mapper<LongWritable, Text, Text, IntWritable> {
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {			
			String [] text=value.toString().split(" ");
			int num_pairs=0;
			int i=0;
			int j=0;
			for(i=0;i<text.length;i++){
				if(stopwords_array.contains(text[i])){
					continue;
				}
				
				if(!text[i].matches("\\w+$")){
					continue;
				}
				
				num_pairs=0;
				for(j=(i+1);j<text.length;j++){ //gia na apofeygw to eayto toy
					if(stopwords_array.contains(text[j])){
						continue;
					}
					
					if(!text[j].matches("\\w+$")){
						continue;
					}
					
					context.write(new Text(text[i]+","+text[j]),new IntWritable(1));
					num_pairs++;
				}
				context.write(new Text(text[i]+",!"),new IntWritable(num_pairs));
			}
		}
	}
	
	public static class PairsReduce extends Reducer<Text, IntWritable, Text, DoubleWritable> {
		static double frequency;
		@Override
		public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
			int sum=0;
			String [] tmp;
			for (IntWritable value : values){
                sum += value.get();
			}
			
			if(key.toString().matches(".*!")){
				frequency=sum;
			}else{
				tmp=key.toString().split(",");
				context.write(new Text(tmp[0]+" "+tmp[1]),new DoubleWritable(sum/frequency));
			}
		}	
	}
	
	public static class StripesMap extends Mapper<LongWritable, Text, Text, Text> {
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {			
			String [] text=value.toString().split(" ");
			HashMap <String,Integer> hash = null;
			int value_text=0;
			for(int i=0;i<text.length;i++){
				hash=new HashMap <String,Integer>();
				if(stopwords_array.contains(text[i])){
					continue;
				}
				
				if(!text[i].matches("\\w+$")){
					continue;
				}
				for(int j=(i+1);j<text.length;j++){
					if(stopwords_array.contains(text[j])){
						continue;
					}
					
					if(!text[j].matches("\\w+$")){
						continue;
					}
					
					if(hash.containsKey(text[j])){
						value_text=hash.get(text[j]);
						hash.put(text[j], (value_text+1));
					}else{
						hash.put(text[j], 1);
					}	
				}
				context.write(new Text(text[i]),new Text(hash.toString().replaceAll("[=]", ",").replaceAll("[{}]", "").replaceAll(" ", "")));
			}
		}
	}
	
	public static class StripesReduce extends Reducer<Text, Text, Text, DoubleWritable> {
		double frequency;
		@Override
		public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
			String [] text;
			HashMap <String,Integer> hash=new HashMap <String,Integer>();
			int sum=0;
			double cnt=0;
			for (Text value : values) {
				text = value.toString().split(",");

				for (int i = 0; i <= text.length-2 ; i = i + 2) {
					if (hash.containsKey(text[i])) {
						sum = hash.get(text[i]) + Integer.parseInt(text[(i + 1)]);
						hash.put(text[i], sum);
					} else {
						hash.put(text[i], Integer.parseInt(text[(i + 1)]));
					}
					cnt+=Integer.parseInt(text[(i + 1)]);
				}
			}
			double freq;
			double sum1=0;
			for(Entry<String,Integer> entry: hash.entrySet()){
				sum1 +=entry.getValue();
				
			}
			for(Entry<String,Integer> entry: hash.entrySet()){
				freq=entry.getValue();
				context.write(new Text(key+" "+entry.getKey()),new DoubleWritable(freq/sum1));
			}
		}	
	}
    
	public static class SortMap extends Mapper<LongWritable, Text, Text, DoubleWritable> {
		@Override
		protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			String line = value.toString();
			StringTokenizer tokenizer = new StringTokenizer(line);
			String pair1 = tokenizer.nextToken();
			String pair2 = tokenizer.nextToken();
			String freq = tokenizer.nextToken();
			
			context.write( new Text(pair1+","+pair2),new DoubleWritable( Double.parseDouble(freq)) );
		}
	}

	public static class SortReduce extends Reducer<Text, DoubleWritable, Text, Text> {
		TreeSet<DoubleComparable> set_pairs = new TreeSet<>();

		@Override
		protected void reduce(Text key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
			for (DoubleWritable value : values) {
				String tmp = key.toString();
				String[] pairs = tmp.split(",");
				set_pairs.add(new DoubleComparable(Double.parseDouble(value.toString()), pairs[0], pairs[1]));

				if (set_pairs.size() > 100) {
					set_pairs.pollFirst();
				}
			}
		}

		protected void cleanup(Context context) throws IOException,InterruptedException {
			while (!set_pairs.isEmpty()) {
				DoubleComparable pair = set_pairs.pollLast();
				context.write(new Text(pair.word1),new Text(pair.word2+": "+pair.frequency));
			}
		}

	}

	public static ArrayList<String> Create_Array_Stopwords(String filename){
		ArrayList<String> StopWords = new ArrayList<String>();
		String line=null;
		String [] words=null;
		try {
            FileReader fileReader = new FileReader(filename);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
            	words =  line.split(",");
                StopWords.add(words[0]);
            }   
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println("Unable to open file :"+ filename );                
        }
        catch(IOException ex) {
            System.out.println("Error reading file :"+ filename);                  
        }        
		return StopWords;
	}
	
	@SuppressWarnings({ "deprecation" })
	public static void main(String[] args) throws Exception {
		FileUtils.deleteDirectory(new File("/home/cloudera/workspace/Assignments/output"));
		FileUtils.deleteDirectory(new File("/home/cloudera/workspace/Assignments/output1"));
		FileUtils.deleteDirectory(new File("/home/cloudera/workspace/Assignments/output_stripe"));
		FileUtils.deleteDirectory(new File("/home/cloudera/workspace/Assignments/output1_stripe"));
		stopwords_array=Create_Array_Stopwords("stopwords.csv");
		
    	int success1;
    	Configuration conf1 = new Configuration();
        Job job1 = new Job(conf1, "Pairs");
        job1.setJarByClass(bonus.class);
        job1.setOutputKeyClass(Text.class);
        job1.setOutputValueClass(IntWritable.class);
        job1.setMapperClass(PairsMap.class);
        job1.setReducerClass(PairsReduce.class);
        job1.setInputFormatClass(TextInputFormat.class);
        job1.setOutputFormatClass(TextOutputFormat.class);
        
        job1.setNumReduceTasks(1);
        String names=args[0];
        for(int i=1; i<=5; i++){
        	names=args[i]+","+names;
       	}
        
    	System.out.println(names);  
       	FileInputFormat .setInputPaths(job1,names);
       	FileOutputFormat.setOutputPath(job1, new Path(args[6]));
       	success1 = job1.waitForCompletion(true) ? 1 : 0;
		if (success1 == 1) {
			System.out.println(success1);
			Configuration conf2 = new Configuration();
	        Job job2 = new Job(conf2, "Sort Pairs");
	        job2.setJarByClass(bonus.class);
	        job2.setOutputKeyClass(Text.class);
	        job2.setOutputValueClass(DoubleWritable.class);
	        job2.setMapperClass(SortMap.class);
	        job2.setReducerClass(SortReduce.class);
	        job2.setInputFormatClass(TextInputFormat.class);
	        job2.setOutputFormatClass(TextOutputFormat.class);
	        
	        job2.setNumReduceTasks(1);
	         
	       	FileInputFormat .setInputPaths(job2,"./output/part-r-00000");
	       	FileOutputFormat.setOutputPath(job2, new Path(args[7]));
	       	int success2 = job2.waitForCompletion(true) ? 1 : 0;
	       	if(success2 == 1){
	       		System.out.println(success2);
	       	}
		}
		
		//STRIPES
		Configuration conf3 = new Configuration();
        Job job3 = new Job(conf3, "Stripes");
        job3.setJarByClass(bonus.class);
        job3.setOutputKeyClass(Text.class);
        job3.setOutputValueClass(Text.class);
        job3.setMapperClass(StripesMap.class);
        job3.setReducerClass(StripesReduce.class);
        job3.setInputFormatClass(TextInputFormat.class);
        job3.setOutputFormatClass(TextOutputFormat.class);
        
        job3.setNumReduceTasks(1);
        String names3=args[0];
        for(int i=1; i<=5; i++){
        	names3=args[i]+","+names;
       	}
       
    	System.out.println(names3);  
       	FileInputFormat .setInputPaths(job3,names3);
       	FileOutputFormat.setOutputPath(job3, new Path(args[8]));
       	int success3 = job3.waitForCompletion(true) ? 1 : 0;
       	if (success3 == 1) {
			System.out.println(success3);
			Configuration conf2 = new Configuration();
	        Job job4 = new Job(conf2, "Sort Stripes");
	        job4.setJarByClass(bonus.class);
	        job4.setOutputKeyClass(Text.class);
	        job4.setOutputValueClass(DoubleWritable.class);
	        job4.setMapperClass(SortMap.class);
	        job4.setReducerClass(SortReduce.class);
	        job4.setInputFormatClass(TextInputFormat.class);
	        job4.setOutputFormatClass(TextOutputFormat.class);
	        
	        job4.setNumReduceTasks(1);
	         
	       	FileInputFormat .setInputPaths(job4,"./output_stripe/part-r-00000");
	       	FileOutputFormat.setOutputPath(job4, new Path(args[9]));
	       	int success4 = job4.waitForCompletion(true) ? 1 : 0;
	       	if(success4 == 1){
	       		System.out.println(success4);
	       	}
		}
	}
}