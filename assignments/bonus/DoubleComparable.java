package bonus;

public class DoubleComparable implements Comparable<DoubleComparable>{
    public double frequency;
    public String word1;
    public String word2;
    
    DoubleComparable(double frequency, String word1, String word2) {
        this.frequency = frequency;
        this.word1 = word1;
        this.word2 = word2;
    }

    @Override
    public int compareTo(DoubleComparable pair) {
        if (this.frequency >= pair.frequency) {
            return 1;
        } else {
            return -1;
        }
    
    }
	
}
