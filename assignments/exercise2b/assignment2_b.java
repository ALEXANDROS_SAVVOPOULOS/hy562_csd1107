package exercise2b;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class assignment2_b {
	static int id=0;
	static ArrayList<String> stopwords_array=null;	
	
	public static class MyMap extends Mapper<LongWritable, Text, Text, Text> {
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			FileSplit fileName= (FileSplit) context.getInputSplit();
			String file = fileName.getPath().getName();

			String text_string;
			
			String line = value.toString();
			StringTokenizer tokenizer = new StringTokenizer(line);
            while (tokenizer.hasMoreTokens()){
				text_string=tokenizer.nextToken().toString();
				
				if(!stopwords_array.contains(text_string)){
					context.write(new Text(text_string), new Text(file));
				}
			}
		}
	}
	
	public static class MyReduce extends Reducer<Text, Text, Text, Text> {
		@Override
		public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
			HashMap<String,String> hash=new HashMap<String,String>();
			HashMap<Integer,String> hash1=new HashMap<Integer,String>();
			String count="";
			
			if(hash1.get(key.toString())==null){
				hash1.put(id, key.toString());
				id++;
			}
			
			for(Text value:values){
				String str=value.toString();
				if(hash.get(str)!=null){		
					count=hash.get(str);
					hash.put(str,count);
				}else{
					hash.put(str,count);
				}
			}
            context.getCounter("RecordCounter","Reducer-no-"+context.getConfiguration().getInt("mapreduce.task.partition", 0)).increment(1);
			context.write(new Text(hash1.toString().replaceAll("[{]", "").replaceAll("[=}]", " ")), new Text(hash.toString().replaceAll("[{]", "").replaceAll("[=}]", "")));
		}	
	}
	
	public static ArrayList<String> ReadStopWords(String fileName){
		ArrayList<String> StopWords = new ArrayList<String>();
		String line=null;
		String [] Words=null;
		try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
                Words =  line.split(",");
                StopWords.add(Words[0]);
            }   
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file :"+ fileName );                
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file :"+ fileName);                  
        }        
		return StopWords;
	}

	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws Exception {
		FileUtils.deleteDirectory(new File("/home/cloudera/workspace/Assignments/output"));
		FileUtils.deleteDirectory(new File("/home/cloudera/workspace/Assignments/output1"));
		
		stopwords_array=ReadStopWords("stopwords.csv");
		
    	int success;
    	Configuration conf1 = new Configuration();
        Job job1 = new Job(conf1, "simple_inverted_index");
        job1.setJarByClass(assignment2_b.class);
        job1.setOutputKeyClass(Text.class);
        job1.setOutputValueClass(Text.class);
        job1.setMapperClass(MyMap.class);
        job1.setReducerClass(MyReduce.class);
        job1.setInputFormatClass(TextInputFormat.class);
        job1.setOutputFormatClass(TextOutputFormat.class);
        
        job1.setNumReduceTasks(1);
        String names=args[0];
        for(int i=1; i<=5; i++){
        	names=args[i]+","+names;
       	}
        
    	System.out.println(names);  
       	FileInputFormat .setInputPaths(job1,names);

       	FileOutputFormat.setOutputPath(job1, new Path(args[6]));
       	success = job1.waitForCompletion(true) ? 1 : 0;
		if (success == 1) {
			System.out.println(success); 
			System.out.println(job1.getCounters());
		}
		System.out.println(id);
	}
}