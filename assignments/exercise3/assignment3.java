package exercise3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class assignment3 {
	static int id=0;
	static ArrayList<String> stopwords_array=null;	
	
	public static class MyMap extends Mapper<LongWritable, Text, Text, Text> {
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			FileSplit fileName= (FileSplit) context.getInputSplit();
			String file = fileName.getPath().getName();
			
			Text text = new Text();
			String text_string;
			
			String line = value.toString();
			StringTokenizer tokenizer = new StringTokenizer(line);
            while (tokenizer.hasMoreTokens()){
				text_string=tokenizer.nextToken().toString();
				
				if(!stopwords_array.contains(text_string)){
					text.set(text_string);
					context.write(text, new Text(file));
				}
			}
		}
	}
	
	public static class MyReduce extends Reducer<Text, Text, Text, Text> {
		@Override
		public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
			HashMap<String,Integer> hash=new HashMap<String,Integer>();
			int count;
			int ein =1;
			
			for(Text value:values){
				String str=value.toString();
				if(hash!=null && hash.get(str)!=null){		
					count=hash.get(str);
					hash.put(str,count+ein);
				}else{
					hash.put(str,ein);
				}
			}
			context.write(new Text(key.toString()), new Text(hash.toString().replaceAll("[{]", "").replaceAll("[=}]", " ")));
		}	
	}
	
	public static class Map extends Mapper<LongWritable, Text, Text, Text> {
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String line = value.toString();
            StringTokenizer tokenizer = new StringTokenizer(line);
            String key_string;
            String tmp="";
            key_string=tokenizer.nextToken().toString();
            while (tokenizer.hasMoreTokens()) {
            	tmp+=" "+tokenizer.nextToken().toString();
            }
            context.write(new Text(key_string),new Text(tmp));
        }
    }
	
	public static class Reduce extends Reducer<Text, Text, Text, Text> {
		protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
			HashMap<String,String> hash=new HashMap<String,String>();
			
			if(hash.get(key.toString())== null ){
				hash.put(key.toString(), "");
			}
			
			String tmp="";
			String [] tmp_array;
			String [] tmp1_array;
			for (Text value : values) {
				tmp+=value.toString();
			}
			
			tmp_array=tmp.split(",");
			tmp="";
			for(int i=0;i<tmp_array.length;i++){ //bgazei toys assoys
				tmp+=tmp_array[i].substring(0, tmp_array[i].length()-1);
			}
			
			tmp1_array=tmp.split(" ");
			for(int i=0;i<tmp1_array.length;i++){
				tmp+=" "+tmp1_array[i];	
			}
			hash.put(key.toString(),tmp);
			id++;
			context.write(new Text(Integer.toString(id)), new Text (hash.toString().replaceAll("[{]", "").replaceAll("[=}]", " ")));
		}
	}

	public static ArrayList<String> Create_Array_Stopwords(String filename){
		ArrayList<String> StopWords = new ArrayList<String>();
		String line=null;
		String [] words=null;
		try {
            FileReader fileReader = new FileReader(filename);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
            	words =  line.split(",");
                StopWords.add(words[0]);
            }   
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println("Unable to open file :"+ filename );                
        }
        catch(IOException ex) {
            System.out.println("Error reading file :"+ filename);                  
        }        
		return StopWords;
	}
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws Exception {
		FileUtils.deleteDirectory(new File("/home/cloudera/workspace/Assignments/output"));
		FileUtils.deleteDirectory(new File("/home/cloudera/workspace/Assignments/output1"));
		
		stopwords_array=Create_Array_Stopwords("stopwords.csv");
		
    	int success;
    	Configuration conf1 = new Configuration();
        Job job1 = new Job(conf1, "first_step_inverted_index");
        job1.setJarByClass(assignment3.class);
        job1.setOutputKeyClass(Text.class);
        job1.setOutputValueClass(Text.class);
        job1.setMapperClass(MyMap.class);
        job1.setCombinerClass(MyReduce.class);
        job1.setReducerClass(MyReduce.class);
        job1.setInputFormatClass(TextInputFormat.class);
        job1.setOutputFormatClass(TextOutputFormat.class);
        
        job1.setNumReduceTasks(1);
        String names=args[0];
        for(int i=1; i<=5; i++){
        	names=args[i]+","+names;
       	}
        
    	System.out.println(names);  
       	FileInputFormat .setInputPaths(job1,names);
       	FileOutputFormat.setOutputPath(job1, new Path(args[6]));
       	success = job1.waitForCompletion(true) ? 1 : 0;
		if (success == 1) {
			System.out.println(success);
			Configuration conf2 = new Configuration();
	        Job job2 = new Job(conf2, "final_step_inverted_index");
	        job2.setJarByClass(assignment3.class);
	        job2.setOutputKeyClass(Text.class);
	        job2.setOutputValueClass(Text.class);
	        job2.setMapperClass(Map.class);
	        job2.setReducerClass(Reduce.class);
	        job2.setInputFormatClass(TextInputFormat.class);
	        job2.setOutputFormatClass(TextOutputFormat.class);
	        job2.setNumReduceTasks(1);
	        
	        FileInputFormat .setInputPaths(job2,"./output/part-r-00000");
	       	FileOutputFormat.setOutputPath(job2, new Path(args[7]));
	       	success = job2.waitForCompletion(true) ? 1 : 0;
	       	if(success==1){
	       		System.out.println(success);
	       	}
	        
		}
		System.out.println(id);
	}
}